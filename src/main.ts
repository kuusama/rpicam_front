import Vue from 'vue';
import App from './App.vue';
import VueResource from 'vue-resource';

import router from './router';
import './registerServiceWorker';
import MdButton from 'vue-material';
import MdDialog from 'vue-material';
import MdCard from 'vue-material';
import MdSwitch from 'vue-material';
import MdTitle from 'vue-material';
// import MdContent from 'vue-material';
// import MdTabs from 'vue-material';
import 'vue-material/dist/vue-material.min.css';

Vue.use(VueResource);
Vue.use(MdButton);
Vue.use(MdDialog);
Vue.use(MdCard);
Vue.use(MdSwitch);
Vue.use(MdTitle);

Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
