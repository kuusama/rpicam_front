export class User {
    public id:       number = 0; 
    public login:    string = ""; 
    public email:    string = ""; 
    public password: string = ""; 
    public active:   number = 0;
    public token:    string = "";
    public level:    number = 0;
}