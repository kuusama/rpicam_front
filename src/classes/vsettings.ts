export class VSettings {
    width     : number = 640;
    height    : number = 360;
    bitrate   : number = 500000;
    framerate : number = 15;
    rotation  : number = 0;
    hflip     : boolean = false;
    vflip     : boolean = true;
    profile   : string = "baseline";
    level     : string = "4";
    hls_time      : number = 2;
    hls_list_size : number = 5;
    hls_wrap      : number = 15;
}