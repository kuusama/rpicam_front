import { Vue } from 'vue-property-decorator';
import { Command } from './command';

export class Network extends Vue {
  public sendCommand(command: Command): PromiseLike<any> {
      let formData = new FormData();
      formData.append('command', JSON.stringify(command));
      return this.$http.post('http://127.0.0.1:8181/api', formData).then(); //Develop
      //return this.$http.post('/api', formData).then(); //Production
    }
}
