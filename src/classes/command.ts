// import { User } from '../classes/user';
import { VSettings } from '../classes/vsettings';
import { User } from '../classes/user';

enum CommandType {
    StartVideo  = 1,
    StopVideo   = 2,
    GetSettings = 3,
    UpdateSettings = 4,
    Login          = 5,
    AddUser        = 6,
    GetUsers       = 7,
    GetUserData    = 8,
    UpdateUserData = 9,
    DeleteUser     = 10
}

export class Command {
  protected payload!: string;
  private type: CommandType;
  // private userid: number;
  constructor(type: CommandType /*, userid: number*/ ) {
    this.type = type;
    // this.userid = userid;
  }
}

export class StartVideoCommand extends Command {
  constructor() {
    super(CommandType.StartVideo);
  }
}

export class StopVideoCommand extends Command {
  constructor() {
    super(CommandType.StopVideo);
  }
}

export class GetSettingsCommand extends Command {
  constructor() {
    super(CommandType.GetSettings);
  }
}

export class UpdateSettingsCommand extends Command {
  constructor(settings: VSettings) {
    super(CommandType.UpdateSettings);
    this.payload = JSON.stringify(settings);
  }
}

export class LoginCommand extends Command {
  constructor(user: User) {
    super(CommandType.Login);
    this.payload = JSON.stringify(user);
  }
}

export class RegisterCommand extends Command {
  constructor(user: User) {
    super(CommandType.AddUser);
    this.payload = JSON.stringify(user);
  }
}

export class GetUsersCommand extends Command {
  constructor() {
    super(CommandType.GetUsers);
  }
}

export class GetUserDataCommand extends Command {
  constructor(token: string) {
    super(CommandType.GetUserData);
    this.payload = JSON.stringify(token);
  }
}

export class UpdateUserDataCommand extends Command {
  constructor(user: User) {
    super(CommandType.UpdateUserData);
    this.payload = JSON.stringify(user);
  }
}

export class DeleteUserCommand extends Command {
  constructor(token: string) {
    super(CommandType.DeleteUser);
    this.payload = JSON.stringify(token);
  }
}
